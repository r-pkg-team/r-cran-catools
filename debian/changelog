r-cran-catools (1.18.3-1) unstable; urgency=medium

  * Team upload.
  [ Andreas Tille ]
  * Disable reprotest

  [ Charles Plessy ]
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 20 Feb 2025 08:40:44 +0900

r-cran-catools (1.18.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Sep 2021 14:19:41 +0200

r-cran-catools (1.18.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jan 2021 08:55:42 +0100

r-cran-catools (1.18.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jan 2020 08:34:54 +0100

r-cran-catools (1.17.1.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 15 Jan 2020 11:34:26 +0100

r-cran-catools (1.17.1.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Fri, 06 Dec 2019 16:04:31 +0100

r-cran-catools (1.17.1.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/changelog

 -- Dylan Aïssi <daissi@debian.org>  Fri, 19 Jul 2019 10:09:09 +0200

r-cran-catools (1.17.1.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Tue, 24 Jul 2018 08:46:08 +0200

r-cran-catools (1.17.1-3) unstable; urgency=medium

  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jun 2018 16:45:23 +0200

r-cran-catools (1.17.1-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * debhelper 10
  * Convert from cdbs to dh-r
  * Secure URI in watch file
  * Canonical homepage for CRAN packages
  * Review d/control
  * Testsuite: autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Thu, 19 Oct 2017 10:38:32 +0200

r-cran-catools (1.17.1-1) unstable; urgency=medium

  * New upstream version
  * reviewed d/copyright

 -- Andreas Tille <tille@debian.org>  Tue, 16 Sep 2014 18:09:05 +0200

catools (1.17-1) unstable; urgency=low

  [ Charles Plessy ]
  * debian/control: removed myself from Uploaders.

  [ Andreas Tille ]
  * New upstream version
  * Add myself to Uploaders
  * debian/source/format: 3.0 (quilt)
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Tue, 08 Jul 2014 20:50:01 +0200

catools (1.14-1) unstable; urgency=low

  * New upstream release.
  * debian/control: normalised VCS URLs.
  * debian/control: removed obsolete field DM-Upload-Allowed.

 -- Charles Plessy <plessy@debian.org>  Tue, 02 Apr 2013 07:21:31 +0900

catools (1.13-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright file updated to latest machine-readable format.
  * Incremented Standards-Version to reflect conformance with Policy 3.9.4.
    (debian/control, no changes needed).
  * debian/rules: obtain ${R:Depends} directly throuhg r-base-dev.
  * debian/control: build-depend on r-base-dev (>= 2.14.2~20120222), see above.
  * Use Debhelper 9 (debian/control, debian/compat).

 -- Charles Plessy <plessy@debian.org>  Fri, 21 Sep 2012 14:33:02 +0900

catools (1.12-2) unstable; urgency=low

  * Uploaded to rebuild against R 2.14 (Closes: #645832)
  * Corrected Vcs-Browser field in debian/control.
  * Added missing copyright statement in debian/copyright.

 -- Charles Plessy <plessy@debian.org>  Wed, 19 Oct 2011 10:29:06 +0900

catools (1.12-1) unstable; urgency=low

  * New upstream version.
  * Added myself as Uploader (debian/control).
  * Incremented Standards-Version to reflect conformance with Policy 3.9.2.
    (debian/control, no changes needed).

 -- Charles Plessy <plessy@debian.org>  Thu, 02 Jun 2011 12:41:20 +0900

catools (1.11-1) unstable; urgency=low

  * Team upload.
  * New upstream version.
  * debian/copyright:
    - corrected GPL-3 long description (not GPL-3+);
    - converted to DEP-5 format.
  * Corrected Vcs-Svn field in debian/control.
  * Use Debhelper 8 (debian/control, debian/compat).
  * Renamed README.Debian README.test to better reflect its contents.
  * Incremented Standards-Version to reflect conformance with Policy 3.9.1.
    (debian/control, no changes needed).

 -- Charles Plessy <plessy@debian.org>  Tue, 22 Mar 2011 13:24:12 +0900

catools (1.10-1) unstable; urgency=low

  * Team upload (Closes: #577296).
  * New upstream version.
  * Update to Policy 3.8.4:
    - Changed section to ‘gnu-r’ (debian/control).
    - Incremented Standards-Version (debian/control).
  * Collaborative Debian Med maintenance (debian/control):
    - Set our mailing list as Maintainer, move Steffen to Uploaders.
    - Allow upload by Debian Maintainers.
  * Switched to Debhelper 7 (debian/control, debian/compat).
  * Depend on a version equal or superior than the R upstream release that
    was used to build this package, using a R:Depends substvar
    debian/control, debian/rules).
  * Removed mentions of copyright notices that do not exist anymore in
    the upstream code (debian/copyright, debian/README.Debian).
  * Added notes to explain how this package is tested (debian/README.Debian).

 -- Charles Plessy <plessy@debian.org>  Mon, 12 Apr 2010 10:02:22 +0900

catools (1.9-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Mon, 01 Dec 2008 23:41:53 +0100

catools (1.8-1) unstable; urgency=low

  * Initial release (Closes: #487956).

 -- Steffen Moeller <moeller@debian.org>  Wed, 25 Jun 2008 14:29:07 +0200
